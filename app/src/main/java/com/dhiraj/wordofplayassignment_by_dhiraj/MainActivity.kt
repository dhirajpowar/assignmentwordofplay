package com.dhiraj.wordofplayassignment_by_dhiraj

import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.dhiraj.wordofplayassignment_by_dhiraj.databinding.ActivityMainBinding
import com.dhiraj.wordofplayassignment_by_dhiraj.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.activity_main.*
import java.util.regex.Pattern

class MainActivity : AppCompatActivity() {

    private val TAG = MainActivity::class.java.simpleName
    private var emailFlag = false
    private var passwordFlag = false
    var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

         binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        initialize()

        themeSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                emailTextInput.hintTextColor = getColorStateList(android.R.color.white)
                passwordTextInput.hintTextColor = getColorStateList(android.R.color.white)

                inEmail.setTextAppearance(R.style.edittext_grey)
                inPassword.setTextAppearance(R.style.edittext_grey)

                loginLayout.setBackgroundColor(resources.getColor(android.R.color.darker_gray))
            } else {
                emailTextInput.hintTextColor = getColorStateList(android.R.color.darker_gray)
                passwordTextInput.hintTextColor = getColorStateList(android.R.color.darker_gray)

                inEmail.setTextAppearance(R.style.edittext_white)
                inPassword.setTextAppearance(R.style.edittext_white)

                loginLayout.setBackgroundColor(resources.getColor(android.R.color.white))
            }
        }
    }

    private fun initialize() {

        button.isEnabled = false

        val viewModel = LoginViewModel()
        binding?.loginViewModel = viewModel

        inEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (isEmailValid(s.toString())) {
                        emailTextInput.error = null
                        emailFlag = true
                        if (emailFlag and passwordFlag) {
                            button.isEnabled = true
                        } else {
                            button.isEnabled = false
                        }

                    } else {
                        emailTextInput.error = "Enter a valid email address"
                        emailFlag = false
                        button.isEnabled = false
                    }
            }

        })

        inPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(isPasswordValid(s.toString())) {
                    passwordFlag = true
                    if (emailFlag and passwordFlag) {
                        button.isEnabled = true
                    } else {
                        button.isEnabled = false
                    }
                } else {
                    passwordFlag = false
                    button.isEnabled = false
                }
            }

        })

    }

    private fun isPasswordValid(password: String): Boolean {
        var localFlag = true

        var upperCasePattern = Pattern.compile("[A-Z ]")
        var lowerCasePattern = Pattern.compile("[a-z ]")
        var digitCasePattern = Pattern.compile("[0-9 ]")
        var specialCharPattern = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)

        if (password.length < 8) {
            localFlag = false
            passwordTextInput.error = "Password should be greater than 8"
        } else if (password.length > 16) {
            localFlag = false
            passwordTextInput.error = "Password should not be greater than 16"
        } else  if (!upperCasePattern.matcher(password).find())  {
            localFlag = false
            passwordTextInput.error = "Password should have at least one upper case character"
        } else  if (!lowerCasePattern.matcher(password).find()) {
            localFlag = false
            passwordTextInput.error = "Password should have at least one lower case character"
        } else   if (!digitCasePattern.matcher(password).find()) {
            localFlag = false
            passwordTextInput.error = "Password should contain at least one number"
        } else if(!specialCharPattern.matcher(password).find()) {
            localFlag = false
            passwordTextInput.error = "Password should have at least one special character"
        } else {
            passwordTextInput.error = null
        }

        return localFlag
    }


    fun isEmailValid(email:String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

}
