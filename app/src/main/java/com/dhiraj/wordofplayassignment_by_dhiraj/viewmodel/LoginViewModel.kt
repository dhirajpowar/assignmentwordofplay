package com.dhiraj.wordofplayassignment_by_dhiraj.viewmodel

import androidx.annotation.NonNull
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dhiraj.wordofplayassignment_by_dhiraj.BR
import com.dhiraj.wordofplayassignment_by_dhiraj.model.User
import org.jetbrains.annotations.NonNls
import java.util.logging.Handler

class LoginViewModel : ViewModel() {
    public var email: MutableLiveData<String> = MutableLiveData();
    public var password: MutableLiveData<String> = MutableLiveData();
    public val errorPassword: MutableLiveData<String> = MutableLiveData()
    public val errorEmail: MutableLiveData<String> = MutableLiveData()

    private var userMutableLiveData: MutableLiveData<User>? = null

    fun getUser(): LiveData<User> {
        if(userMutableLiveData == null) {
            userMutableLiveData = MutableLiveData()
        }

        return userMutableLiveData!!
    }



    fun onLoginClicked() {
        android.os.Handler().postDelayed(Runnable {
            val user = User(email?.value!!, password?.value!!)

           /* if (!user.isEmailValid()) {
                errorEmail.value = "Enter a valid email address"
            } else {
                errorEmail.value = null
            }

            if (!user.isPasswordLengthGreaterThan8()) {
                errorPassword.value = "Password Length should be greater than 8"
            } else {
                errorPassword.value = null
            }
*/
            userMutableLiveData?.value = user

        }, 5000)
    }
}